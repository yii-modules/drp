<?php
/**
 * Drp 
 *
 * Wrapper for https://github.com/dangrossman/bootstrap-daterangepicker 
 * 
 * @author vi mark <webvimark@gmail.com> 
 * @license MIT
 */
class Drp extends CWidget
{
        /**
         * Which css file load 
         * 
         * @var int - 2|3
         */
        public $bootstrapVersion = 2;
        /**
         * @var string
         */
        public $selector;
        /**
         * @var string
         */
        public $model;
        /**
         * @var string
         */
        public $attribute;
        /**
         * Datepicker params 
         * 
         * @var array
         */
        public $params = array();

        public $applyCallback = '';


        private $_selector;
        private $_params = array(
                'opens'          => 'left',
                'format'         => 'YYYY-MM-DD H:mm',
                'startDayOfWeek' => 1,
        );


        /**
         * init 
         */
        public function init()
        {
                if ( $this->selector )
                {
                        $this->_selector = $this->selector;
                }
                elseif ( $this->model AND $this->attribute ) 
                {
                        $this->_selector = 'input[name="' . $this->model .'[' . $this->attribute . ']"]';
                } 

                if ( ! $this->_selector )
                        throw new Exception('Define selector or model + attributes');

                $this->_registerAssets();


                // If applyCallback not set, then we try to update given grid
                if ( ! $this->applyCallback AND $this->model AND $this->attribute AND $this->selector ) 
                {
                        $this->applyCallback = "$('input[name=\"{$this->model}[{$this->attribute}]\"]').val(picker.startDate.format('{$this->_params['format']}') + ' - ' + picker.endDate.format('{$this->_params['format']}'));";
                        $this->applyCallback .= "$('input[name=\"{$this->model}[{$this->attribute}]\"]').trigger('change');";
                }


                Yii::app()->clientScript->registerScript("drp_{$this->_selector}", "
                        $(document).ready(function() {
                                $(document).on('mouseup', '{$this->_selector}', function(){
                                        $(this).daterangepicker({$this->_mergeParams()});
                                });
                                $(document).on('apply','{$this->_selector}', function(ev, picker) {
                                        $('{$this->_selector}').trigger('change');
                                        {$this->applyCallback};
                                });
                        });
                ");
        }

        /**
         * _registerAssets 
         */
        private function _registerAssets()
        {
                $dir = CHtml::asset(__DIR__ . '/assets');
                $cs = Yii::app()->clientScript;

                $cs->registerScriptFile($dir . '/moment.min.js');
                $cs->registerScriptFile($dir . '/daterangepicker.js');

                if ( $this->bootstrapVersion == 2 ) 
                        $cs->registerCssFile($dir . '/daterangepicker-bs2.css');
                else 
                        $cs->registerCssFile($dir . '/daterangepicker-bs3.css');
        }

        /**
         * _mergeParams 
         * 
         * @return json array
         */
        private function _mergeParams()
        {
                return json_encode(CMap::mergeArray($this->_params, $this->params));
        }
}
